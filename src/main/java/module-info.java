module com.tetris.tetris {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;


    exports com.tetris.gui;
    opens com.tetris.gui to javafx.fxml;
    exports com.tetris;
    opens com.tetris to javafx.fxml;
}