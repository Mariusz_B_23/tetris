package com.tetris;

import com.tetris.gui.GameController;
import com.tetris.gui.GuiController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        URL location = getClass().getClassLoader().getResource("gameLayout.fxml");
        ResourceBundle resource = null;
        FXMLLoader fxmlLoader = new FXMLLoader(location, resource);
        Parent parent = fxmlLoader.load();
        GuiController guiController = fxmlLoader.getController();

        stage.setTitle("Tetris");
        Scene scene = new Scene(parent, 440, 550);
        stage.setScene(scene);
        stage.show();

        new GameController(guiController);
    }

}