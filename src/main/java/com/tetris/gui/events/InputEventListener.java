package com.tetris.gui.events;

import com.tetris.logic.DownData;
import com.tetris.logic.ViewData;

public interface InputEventListener {

    DownData onDownEvent(MoveEvent moveEvent);

    ViewData onLeftEvent();

    ViewData onRightEvent();

    ViewData onRotateEvent();

}