package com.tetris.gui;

import com.tetris.gui.events.EventSource;
import com.tetris.gui.events.InputEventListener;
import com.tetris.gui.events.MoveEvent;
import com.tetris.logic.ClearRow;
import com.tetris.logic.DownData;
import com.tetris.logic.SimpleBoard;
import com.tetris.logic.ViewData;

public class GameController implements InputEventListener {

    private final SimpleBoard simpleBoard = new SimpleBoard(25, 10);
    private final GuiController viewController;


    public GameController(GuiController guiController) {
        this.viewController = guiController;
        this.viewController.setInputEventListener(this);
        simpleBoard.createNewBrick();
        this.viewController.initGameView(simpleBoard.getBoardMatrix(), simpleBoard.getViewData());
        this.viewController.bindScore(simpleBoard.getScore().scoreProperty());
    }

    @Override
    public DownData onDownEvent(MoveEvent event) {
        boolean canMove = simpleBoard.moveBrickDown();
        ClearRow clearRow = null;
        if (!canMove) {
            simpleBoard.mergeBrickToBackground();
            clearRow = simpleBoard.clearRows();
            if (clearRow.getLinesRemoved() > 0) {
                simpleBoard.getScore().add(clearRow.getScoreBonus());
            }

            if (simpleBoard.createNewBrick()) {
                viewController.gameOver();
            }

        } else {
            if (event.getEventSource() == EventSource.USER) {
                simpleBoard.getScore().add(1);
            }
        }
        viewController.refreshGameBackground(simpleBoard.getBoardMatrix());
        return new DownData(clearRow, simpleBoard.getViewData());
    }

    @Override
    public ViewData onLeftEvent() {
        simpleBoard.moveBrickLeft();
        return simpleBoard.getViewData();
    }

    @Override
    public ViewData onRightEvent() {
        simpleBoard.moveBrickRight();
        return simpleBoard.getViewData();
    }

    @Override
    public ViewData onRotateEvent() {
        simpleBoard.rotateBrickLeft();
        return simpleBoard.getViewData();
    }

}