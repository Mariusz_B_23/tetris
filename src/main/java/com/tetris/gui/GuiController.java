package com.tetris.gui;

import com.tetris.gui.events.EventSource;
import com.tetris.gui.events.EventType;
import com.tetris.gui.events.InputEventListener;
import com.tetris.gui.events.MoveEvent;
import com.tetris.logic.DownData;
import com.tetris.logic.ViewData;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.control.ToggleButton;
import javafx.scene.effect.Reflection;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.net.URL;
import java.util.ResourceBundle;

public class GuiController implements Initializable {

    private static final int BRICK_SIZE = 20;
    Timeline timeline;
    private InputEventListener inputEventListener;
    private Rectangle[][] displayMatrix;
    private Rectangle[][] rectangles;

    private final BooleanProperty paused = new SimpleBooleanProperty();
    private final BooleanProperty isGameOver = new SimpleBooleanProperty();

    @FXML
    private ToggleButton pauseButton;

    @FXML
    private GridPane gamePanel;

    @FXML
    private GridPane nextBrick;

    @FXML
    private GridPane brickPanel;

    @FXML
    private Text scoreValue;

    @FXML
    private Group groupNotification;

    @FXML
    private GameOverPanel gameOverPanel;

    public void initGameView(int[][] boardMatrix, ViewData viewData) {
        displayMatrix = new Rectangle[boardMatrix.length][boardMatrix[0].length];
        for (int i = 2; i < boardMatrix.length; i++) {
            for (int j = 0; j < boardMatrix[i].length; j++) {
                Rectangle rectangle = new Rectangle(BRICK_SIZE, BRICK_SIZE);
                rectangle.setFill(Color.TRANSPARENT);
                displayMatrix[i][j] = rectangle;
                gamePanel.add(rectangle, j, i - 2);
            }
        }
        rectangles = new Rectangle[viewData.getBrickData().length][viewData.getBrickData()[0].length];
        for (int i = 0; i < viewData.getBrickData().length; i++) {
            for (int j = 0; j < viewData.getBrickData()[i].length; j++) {
                Rectangle rectangle = new Rectangle(BRICK_SIZE, BRICK_SIZE);
                rectangle.setFill(getFillColor(viewData.getBrickData()[i][j]));
                rectangles[i][j] = rectangle;
                brickPanel.add(rectangle, j, i);
            }
        }
        brickPanel.setLayoutX(gamePanel.getLayoutX() + viewData.getxPosition() * BRICK_SIZE);
        brickPanel.setLayoutY(-42 + gamePanel.getLayoutY() + (viewData.getyPosition() * BRICK_SIZE) +
                viewData.getyPosition());
        generatePreviewPanel(viewData.getNextBrickData());
        timeline = new Timeline(new KeyFrame(Duration.millis(400),
                ae -> moveDown(new MoveEvent(EventType.DOWN, EventSource.THREAD))));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    private void generatePreviewPanel(int[][] nextBrickData) {
        nextBrick.getChildren().clear();
        for (int i = 0; i < nextBrickData.length; i++) {
            for (int j = 0; j < nextBrickData[i].length; j++) {
                Rectangle rectangle = new Rectangle(BRICK_SIZE, BRICK_SIZE);
                setRectangleData(nextBrickData[i][j], rectangle);
                if (nextBrickData[i][j] != 0) {
                    nextBrick.add(rectangle, j, i);
                }
            }
        }
    }

    public void refreshGameBackground(int[][] board) {
        for (int i = 2; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                setRectangleData(board[i][j], displayMatrix[i][j]);
            }
        }
    }

    private void setRectangleData(int color, Rectangle rectangle) {
        rectangle.setFill(getFillColor(color));
        rectangle.setArcHeight(9);
        rectangle.setArcWidth(9);
    }

    public void bindScore(IntegerProperty integerProperty) {
        scoreValue.textProperty().bind(integerProperty.asString());
    }

    private void moveDown(MoveEvent moveEvent) {
        DownData downData = inputEventListener.onDownEvent(moveEvent);
        if (downData.getClearRow() != null && downData.getClearRow().getLinesRemoved() > 0) {
            NotificationPanel notificationPanel = new NotificationPanel(
                    " + " + downData.getClearRow().getScoreBonus());
            groupNotification.getChildren().add(notificationPanel);
            notificationPanel.showScore(groupNotification.getChildren());
        }
        refreshBrick(downData.getViewData());
    }

    private void refreshBrick(ViewData viewData) {
        brickPanel.setLayoutX(gamePanel.getLayoutX() + viewData.getxPosition() * BRICK_SIZE);
        brickPanel.setLayoutY(-42 + gamePanel.getLayoutY() + (viewData.getyPosition() * BRICK_SIZE) +
                viewData.getyPosition());
        for (int i = 0; i < viewData.getBrickData().length; i++) {
            for (int j = 0; j < viewData.getBrickData()[i].length; j++) {
                setRectangleData(viewData.getBrickData()[i][j], rectangles[i][j]);
            }
        }
        generatePreviewPanel(viewData.getNextBrickData());
    }

    public void setInputEventListener(InputEventListener inputEventListener) {
        this.inputEventListener = inputEventListener;
    }

    public Paint getFillColor(int i) {
        Paint returnPaint;
        switch (i) {
            case 0 -> returnPaint = Color.TRANSPARENT;
            case 1 -> {
                Image grass1 = new Image("grass1.png");
                returnPaint = new ImagePattern(grass1);
            }
            case 2 -> {
                Image water4 = new Image("water4.png");
                returnPaint = new ImagePattern(water4);
            }
            case 3 -> {
                Image grass2 = new Image("grass2.png");
                returnPaint = new ImagePattern(grass2);
            }
            case 4 -> {
                Image rocks1 = new Image("rocks1.png");
                returnPaint = new ImagePattern(rocks1);
            }
            case 5 -> {
                Image water1 = new Image("water1.png");
                returnPaint = new ImagePattern(water1);
            }
            case 6 -> {
                Image water2 = new Image("water2.png");
                returnPaint = new ImagePattern(water2);
            }
            case 7 -> {
                Image water3 = new Image("water3.png");
                returnPaint = new ImagePattern(water3);
            }
            default -> returnPaint = Color.WHITE;
        }
        return returnPaint;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        gamePanel.setFocusTraversable(true);
        gamePanel.requestFocus();
        gamePanel.setOnKeyPressed(keyEvent -> {
            if (paused.getValue() == Boolean.FALSE && isGameOver.getValue() == Boolean.FALSE) {
                if (keyEvent.getCode() == KeyCode.UP || keyEvent.getCode() == KeyCode.W) {
                    refreshBrick(inputEventListener.onRotateEvent());
                    keyEvent.consume();
                }
                if (keyEvent.getCode() == KeyCode.DOWN || keyEvent.getCode() == KeyCode.S) {
                    moveDown(new MoveEvent(EventType.DOWN, EventSource.USER));
                    keyEvent.consume();
                }
                if (keyEvent.getCode() == KeyCode.LEFT || keyEvent.getCode() == KeyCode.A) {
                    refreshBrick(inputEventListener.onLeftEvent());
                    keyEvent.consume();
                }
                if (keyEvent.getCode() == KeyCode.RIGHT || keyEvent.getCode() == KeyCode.D) {
                    refreshBrick(inputEventListener.onRightEvent());
                    keyEvent.consume();
                }
            }
            if (keyEvent.getCode() == KeyCode.P) {
                pauseButton.selectedProperty().setValue(!pauseButton.selectedProperty().getValue());
            }
        });
        gameOverPanel.setVisible(false);
        pauseButton.selectedProperty().bindBidirectional(paused);
        pauseButton.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    timeline.pause();
                    pauseButton.setText("Resume");
                } else {
                    timeline.play();
                    pauseButton.setText("Pause");
                }
            }
        });

        Reflection reflection = new Reflection();
        reflection.setFraction(0.8);
        reflection.setTopOpacity(0.9);
        reflection.setTopOffset(-12);
        scoreValue.setEffect(reflection);
    }

    public void gameOver() {
        timeline.stop();
        isGameOver.setValue(Boolean.TRUE);
        gameOverPanel.setVisible(true);
    }

}