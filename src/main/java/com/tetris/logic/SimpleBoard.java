package com.tetris.logic;

import com.tetris.logic.bricks.Brick;
import com.tetris.logic.bricks.RandomBrickGenerator;

import java.awt.*;

public class SimpleBoard {

    private final int width;
    private final int height;
    private int[][] currentGameMatrix;
    private final RandomBrickGenerator brickGenerator;
    private Brick brick;
    private int currentShape = 0;
    private Point currentOffset;
    private Score score;


    public SimpleBoard(int width, int height) {
        this.width = width;
        this.height = height;
        currentGameMatrix = new int[width][height];
        brickGenerator = new RandomBrickGenerator();
        score = new Score();
    }

    public boolean createNewBrick() {
        currentShape = 0;
        Brick currentBrick = brickGenerator.getBrick();
        setBrick(currentBrick);
        currentOffset = new Point(4, 0);
        return MatrixOperations.intersects(currentGameMatrix, getCurrentShape(),
                currentOffset.x, currentOffset.y);
    }

    public boolean moveBrickDown() {
        Point point = new Point(currentOffset);
        point.translate(0, 1);
        currentOffset = point;
        boolean conflict = MatrixOperations.intersects(currentGameMatrix, getCurrentShape(),
                point.x, point.y);
        if (conflict) {
            return false;
        } else {
            currentOffset = point;
            return true;
        }
    }

    public boolean moveBrickLeft() {
        Point point = new Point(currentOffset);
        point.translate(-1, 0);
        boolean conflict = MatrixOperations.intersects(currentGameMatrix, getCurrentShape(),
                point.x, point.y);
        if (conflict) {
            return false;
        } else {
            currentOffset = point;
            return true;
        }
    }

    public boolean moveBrickRight() {
        Point point = new Point(currentOffset);
        point.translate(1, 0);
        boolean conflict = MatrixOperations.intersects(currentGameMatrix, getCurrentShape(),
                point.x, point.y);
        if (conflict) {
            return false;
        } else {
            currentOffset = point;
            return true;
        }
    }

    public ViewData getViewData() {
        return new ViewData(getCurrentShape(), currentOffset.x, currentOffset.y,
                brickGenerator.getNextBrick().getBrickMatrix().get(0));
    }

    public int[][] getCurrentShape() {
        return this.brick.getBrickMatrix().get(currentShape);
    }

    public int[][] getBoardMatrix() {
        return currentGameMatrix;
    }

    public void setBrick(Brick brick) {
        this.brick = brick;
        currentOffset = new Point(4, 0);
    }

    public Score getScore() {
        return score;
    }

    public void mergeBrickToBackground() {
        currentGameMatrix = MatrixOperations.merge(currentGameMatrix, getCurrentShape(),
                currentOffset.x, currentOffset.y);

    }

    public NextShapeInfo getNextShape() {
        int nextShape = currentShape;
        nextShape = ++nextShape % brick.getBrickMatrix().size();
        return new NextShapeInfo(brick.getBrickMatrix().get(nextShape), nextShape);
    }

    public boolean rotateBrickLeft() {
        NextShapeInfo nextShapeInfo = getNextShape();
        boolean conflict = MatrixOperations.intersects(currentGameMatrix,
                nextShapeInfo.getShape(), currentOffset.x, currentOffset.y);
        if (conflict) {
            return false;
        } else {
            setCurrentShape(nextShapeInfo.getPosition());
            return true;
        }
    }

    public void setCurrentShape(int currentShape) {
        this.currentShape = currentShape;
    }

    public ClearRow clearRows() {
        ClearRow clearRow = MatrixOperations.checkRemoving(currentGameMatrix);
        currentGameMatrix = clearRow.getNextMatrix();
        return clearRow;
    }

}